<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB as DB;
use Illuminate\Http\Request;

class BookController extends Controller
{
    public function index()
    {

        $book = DB::table('books')->select()->get();

        return response()->json($book,200);

    }
}
